import React from "react";

class ConferenceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      starts: "",
      ends: "",
      description: "",
      maximumPresentations: "",
      maximumAttendees: "",
      locations: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8000/api/locations/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handleStartsChange = this.handleStartsChange.bind(this);
      this.handleEndsChange = this.handleEndsChange.bind(this);
      this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
      this.handleMaximumAttendeesChange =
        this.handleMaximumAttendeesChange.bind(this);
      this.handleMaximumPresentationsChange =
        this.handleMaximumPresentationsChange.bind(this);
      this.handleLocationChange = this.handleLocationChange.bind(this);
    }
  }

  async handleSubmit(e) {
    e.preventDefault();
    const data = { ...this.state };
    data.max_presentations = data.maximumPresentations;
    data.max_attendees = data.maximumAttendees;
    delete data.maximumPresentations;
    delete data.maximumAttendees;
    delete data.locations;
    console.log(data);

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      const cleared = {
        name: "",
        starts: "",
        ends: "",
        description: "",
        maximumPresentations: "",
        maximumAttendees: "",
        location: "",
      };

      this.setState(cleared);
    }
  }

  handleNameChange(e) {
    const value = e.target.value;
    this.setState({ name: value });
  }

  handleStartsChange(e) {
    const value = e.target.value;
    this.setState({ starts: value });
  }

  handleEndsChange(e) {
    const value = e.target.value;
    this.setState({ ends: value });
  }

  handleDescriptionChange(e) {
    const value = e.target.value;
    this.setState({ description: value });
  }

  handleMaximumPresentationsChange(e) {
    const value = e.target.value;
    this.setState({ maximumPresentations: value });
  }

  handleMaximumAttendeesChange(e) {
    const value = e.target.value;
    this.setState({ maximumAttendees: value });
  }

  handleLocationChange(e) {
    const value = e.target.value;
    this.setState({ location: value });
  }

  render() {
    console.log(this.state);
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form
              onSubmit={this.handleSubmit.bind(this)}
              id="create-conference-form"
            >
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  required
                  placeholder="Name"
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                  value={this.state.name}
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleStartsChange}
                  required
                  placeholder="Starts"
                  type="date"
                  name="starts"
                  id="starts"
                  className="form-control"
                  value={this.state.starts}
                />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleEndsChange}
                  required
                  placeholder="Ends"
                  type="date"
                  name="ends"
                  id="ends"
                  className="form-control"
                  value={this.state.ends}
                />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">
                  Description
                </label>
                <textarea
                  onChange={this.handleDescriptionChange}
                  required
                  className="form-control"
                  name="description"
                  id="description"
                  rows="3"
                  value={this.state.description}
                ></textarea>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleMaximumPresentationsChange}
                  required
                  placeholder="Maximum Presentations"
                  type="number"
                  name="max_presentations"
                  id="max_presentations"
                  className="form-control"
                  value={this.state.maximumPresentations}
                />
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleMaximumAttendeesChange}
                  required
                  placeholder="Maximum Attendees"
                  type="number"
                  name="max_attendees"
                  id="max_attendees"
                  className="form-control"
                  value={this.state.maximumAttendees}
                />
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleLocationChange}
                  required
                  name="location"
                  id="location"
                  className="form-select"
                  value={this.state.location}
                >
                  <option value="">Choose a location</option>
                  {this.state.locations.map((location) => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ConferenceForm;

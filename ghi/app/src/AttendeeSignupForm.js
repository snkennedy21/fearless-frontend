import React from "react";

class AttendeeSignupForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { conferences: [], name: "", email: "" };
    this.changeConferenceHandler = this.changeConferenceHandler.bind(this);
    this.changeEmailHandler = this.changeEmailHandler.bind(this);
    this.changeNameHandler = this.changeNameHandler.bind(this);
  }

  async componentDidMount() {
    const url = "http://localhost:8000/api/conferences/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ conferences: data.conferences });
    }
  }

  async submitHandler(e) {
    e.preventDefault();
    const data = { ...this.state };
    delete data.conferences;
    console.log(data);

    const attendeesUrl = "http://localhost:8001/api/attendees/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(attendeesUrl, fetchConfig);

    if (response.ok) {
      const newAttendee = await response.json();
      console.log(newAttendee);

      const cleared = {
        name: "",
        email: "",
        conference: "",
      };

      this.setState(cleared);
    }
  }

  changeNameHandler(e) {
    const value = e.target.value;
    this.setState({ name: value });
  }

  changeEmailHandler(e) {
    const value = e.target.value;
    this.setState({ email: value });
  }

  changeConferenceHandler(e) {
    const value = e.target.value;
    this.setState({ conference: value });
  }

  render() {
    let spinnerClasses = "spinner-grow text-secondary";
    let dropdownClasses = "form-select d-none";

    if (this.state.conferences.length > 0) {
      spinnerClasses = "spinner-grow text-secondary d-none";
      dropdownClasses = "form-select";
    }

    return (
      <div className="row">
        <div className="col col-sm-auto">
          <img
            width="300"
            className="bg-white rounded shadow d-block mx-auto mb-4"
            src="/logo.svg"
          />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form
                onSubmit={this.submitHandler.bind(this)}
                id="create-attendee-form"
              >
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference you'd like to attend.
                </p>
                <div
                  className="d-flex justify-content-center mb-3"
                  id="loading-conference-spinner"
                >
                  <div className={spinnerClasses} role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select
                    name="conference"
                    id="conference"
                    className={dropdownClasses}
                    required
                    onChange={this.changeConferenceHandler}
                    value={this.state.conference}
                  >
                    <option value="">Choose a conference</option>
                    {this.state.conferences.map((conference) => {
                      return (
                        <option key={conference.id} value={conference.href}>
                          {conference.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <p className="mb-3">Now, tell us about yourself.</p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input
                        required
                        placeholder="Your full name"
                        type="text"
                        id="name"
                        name="name"
                        className="form-control"
                        onChange={this.changeNameHandler}
                        value={this.state.name}
                      />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input
                        required
                        placeholder="Your email address"
                        type="email"
                        id="email"
                        name="email"
                        className="form-control"
                        onChange={this.changeEmailHandler}
                        value={this.state.email}
                      />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div
                className="alert alert-success d-none mb-0"
                id="success-message"
              >
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AttendeeSignupForm;

import React from "react";
import Nav from "./nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendeeSignupForm from "./AttendeeSignupForm";

function App(props) {
  return (
    <React.Fragment>
      <Nav />
      <div className="container">
        <AttendeeSignupForm />
        {/* <ConferenceForm /> */}
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </React.Fragment>
  );
}

export default App;

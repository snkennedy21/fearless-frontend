function createConferenceOption(conference) {
  const conferenceOption = document.createElement("option");
  conferenceOption.value = conference.id;
  conferenceOption.textContent = conference.name;
  return conferenceOption;
}

window.addEventListener("DOMContentLoaded", async () => {
  const formSelector = document.querySelector("#conference");
  const url = "http://localhost:8000/api/conferences/";
  const response = await fetch(url);
  if (!response.ok) return;
  const data = await response.json();
  console.log(data.conferences);
  const listOfConferences = data.conferences;

  listOfConferences.forEach((conference) => {
    const conferenceOption = createConferenceOption(conference);
    formSelector.appendChild(conferenceOption);
  });
});

window.addEventListener("DOMContentLoaded", async () => {
  const form = document.getElementById("create-presentation-form");

  form.addEventListener("submit", async (e) => {
    e.preventDefault();
    const selectTag = document.querySelector("#conference");
    const conferenceValue = selectTag.value;
    const formData = new FormData(form);
    const json = JSON.stringify(Object.fromEntries(formData));
    const presentationUrl = `http://localhost:8000/api/conferences/${conferenceValue}/presentations/`;

    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(presentationUrl, fetchConfig);
    console.log(response);
    if (!response.ok) return;
    const newPresentation = await response.json();
    form.reset();
    console.log(newPresentation);
  });
});

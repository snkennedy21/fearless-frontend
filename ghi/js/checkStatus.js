// Get the cookie out of the cookie store

const locationLink = document.querySelector("#new-location-link");
const conferenceLink = document.querySelector("#new-conference-link");

const payloadCookie = await cookieStore.get("jwt_access_payload"); // FINISH THIS
if (payloadCookie) {
  console.log(payloadCookie.value);

  // const encodedPayload = JSON.parse(payloadCookie.value);

  // The cookie value is a JSON-formatted string, so parse it
  const encodedPayload = payloadCookie.value;

  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(encodedPayload); // FINISH THIS

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload); // FINISH THIS
  // Print the payload
  console.log(payload);

  if (payload.user.perms.includes("events.add_conference")) {
    conferenceLink.classList.remove("d-none");
  }

  if (payload.user.perms.includes("events.add_location")) {
    locationLink.classList.remove("d-none");
  }

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
}

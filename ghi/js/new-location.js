function createStateOption(state, abbreviation) {
  const stateOption = document.createElement("option");
  stateOption.value = abbreviation;
  stateOption.textContent = state;
  return stateOption;
}

window.addEventListener("DOMContentLoaded", async () => {
  const formSelector = document.querySelector("#state");
  const url = "http://localhost:8000/api/states/";
  const response = await fetch(url);
  if (!response.ok) return;
  const data = await response.json();
  const listOfStates = data.states;

  listOfStates.forEach((state) => {
    const stateName = Object.keys(state);
    const stateAbbreviation = Object.values(state);
    const stateOption = createStateOption(stateName, stateAbbreviation);
    formSelector.appendChild(stateOption);
  });
});

window.addEventListener("DOMContentLoaded", async () => {
  // State fetching code, here...
  const formTag = document.getElementById("create-location-form");

  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    console.log(response);

    if (!response.ok) return;
    form.reset();
    const newLocation = await response.json();
  });
});

function createLocationOption(location) {
  const locationOption = document.createElement("option");
  locationOption.value = location.id;
  locationOption.textContent = location.name;
  return locationOption;
}

window.addEventListener("DOMContentLoaded", async () => {
  const formSelector = document.querySelector("#location");
  const url = "http://localhost:8000/api/locations/";
  const response = await fetch(url);
  if (!response.ok) return;
  const data = await response.json();
  const listOfLocations = data.locations;

  listOfLocations.forEach((location) => {
    const locationOption = createLocationOption(location);
    formSelector.appendChild(locationOption);
  });
});

window.addEventListener("DOMContentLoaded", async () => {
  const formTag = document.getElementById("create-conference-form");

  formTag.addEventListener("submit", async (e) => {
    e.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const conferenceUrl = "http://localhost:8000/api/conferences/";

    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (!response.ok) return;
    form.reset();
    const newConference = await response.json();
  });
});

function createConferenceOption(conference) {
  const conferenceOption = document.createElement("option");
  conferenceOption.value = conference.href;
  conferenceOption.textContent = conference.name;
  console.log(conferenceOption.value);
  return conferenceOption;
}

window.addEventListener("DOMContentLoaded", async () => {
  const formSelector = document.querySelector("#conference");
  const url = "http://localhost:8000/api/conferences/";
  const response = await fetch(url);
  if (!response.ok) return;
  const data = await response.json();
  console.log(data.conferences);
  const listOfConferences = data.conferences;

  listOfConferences.forEach((conference) => {
    const conferenceOption = createConferenceOption(conference);
    formSelector.appendChild(conferenceOption);
  });

  const dropdown = document.querySelector("#conference");
  const spinner = document.querySelector(".spinner-grow");
  spinner.classList.add("d-none");
  dropdown.classList.remove("d-none");

  const form = document.querySelector("#create-attendee-form");

  form.addEventListener("submit", async (e) => {
    e.preventDefault();
    const formData = new FormData(form);
    const json = JSON.stringify(Object.fromEntries(formData));
    const conferenceUrl = "http://localhost:8001/api/attendees/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (!response.ok) return;
    form.reset();
    const newConference = await response.json();

    const successMessage = document.querySelector("#success-message");
    form.classList.add("d-none");
    successMessage.classList.remove("d-none");
  });
});

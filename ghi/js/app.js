function createCard(
  name,
  description,
  pictureUrl,
  startDate,
  endDate,
  location
) {
  return `
    <div class="card shadow p-3 mb-5 bg-white rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">${startDate} - ${endDate}</div>
    </div>
  `;
}

function createPlaceholder() {
  return `
  <div class="col-4">
    <div class="card" aria-hidden="true">
      <img src="..." class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title placeholder-glow">
          <span class="placeholder col-6"></span>
        </h5>
        <h6 class="card-title placeholder-glow">
          <span class="placeholder col-6"></span>
        </h6>
        <p class="card-text placeholder-glow">
          <span class="placeholder col-7"></span>
        </p>
      </div>
      <div class="card-footer placeholder-glow">
        <span class="placeholder col-7"></span>
      </div>
    </div>
  </div>
  `;
}

function padLessThanTen(n) {
  return n < 10 ? "0" + n : n;
}

function triggerAlert(message, type) {
  let alertPlaceholder = document.getElementById("liveAlertPlaceholder");
  let wrapper = document.createElement("div");
  wrapper.innerHTML =
    '<div class="alert alert-' +
    type +
    ' alert-dismissible" role="alert">' +
    message +
    '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
  alertPlaceholder.append(wrapper);
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);
    if (!response.ok) {
      triggerAlert("An error occured while fetching data", "info");
    } else {
      const data = await response.json();
      const row = document.querySelector(".row");

      data.conferences.forEach((conf) => {
        row.innerHTML += createPlaceholder();
      });

      const allPlaceholders = document.querySelectorAll(".col-4");

      for (let [index, conference] of data.conferences.entries()) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (!detailResponse.ok) {
          return;
        }
        console.log(index);
        const details = await detailResponse.json();
        const start = new Date(details.conference.starts);
        const startYear = start.getFullYear();
        const startMonth = padLessThanTen(start.getMonth() + 1);
        const startDay = padLessThanTen(start.getDate());

        const end = new Date(details.conference.ends);
        const endYear = end.getFullYear();
        const endMonth = padLessThanTen(end.getMonth() + 1);
        const endDay = padLessThanTen(end.getDate());

        const startDate = `${startMonth}/${startDay}/${startYear}`;
        const endDate = `${endMonth}/${endDay}/${endYear}`;
        // const startDate = start.toLocaleDateString();
        // const endDate = end.toLocaleDateString();

        // Conference Card Data
        const name = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const location = details.conference.location.name;

        const col4 = document.createElement("div");
        col4.classList.add("col-4");
        col4.innerHTML = createCard(
          name,
          description,
          pictureUrl,
          startDate,
          endDate,
          location
        );
        console.log(col4);
        console.log(allPlaceholders);
        allPlaceholders[index].replaceWith(col4);
      }
    }
  } catch (e) {
    triggerAlert("An Error Occured While Building Your HTML", "info");
  }
});
